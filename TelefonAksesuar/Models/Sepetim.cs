//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TelefonAksesuar.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Sepetim
    {
        public int SepetNo { get; set; }
        public Nullable<int> UyeID { get; set; }
        public string UrunAd { get; set; }
        public Nullable<int> Adet { get; set; }
        public Nullable<int> Fiyat { get; set; }
        public Nullable<int> ToplamFiyat { get; set; }
    
        public virtual Uyeler Uyeler { get; set; }
    }
}
